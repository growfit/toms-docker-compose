1. 前提(インストールしておく)
docker 
docker-compose
git

2. laravel ローカル環境起動
git clone git@bitbucket.org:hogehoge/toms-docker-compose.git
cd toms-docker-compose
docker-compose up -d

3. アプリケーション配置(ある場合)

cd ./server
git clone git@bitbucket.org:hogehoge/toms-admin-frontend
chmod -R 777 ./toms-admin-frontend/storage
exit

4. laravel マイグレート
docker-compose exec php bash
cd /data/www/toms-admin-frontend
composer create-project laravel/laravel toms-admin-frontend # laravel デフォルト（既にコンテンツ配置済みなら不要）
vi .env

```
DB_CONNECTION=mysql
DB_HOST=${DB_HOST}
DB_PORT=3306
DB_DATABASE=${DB_SCHEME}
DB_USERNAME=${DB_APP_USER}
DB_PASSWORD=${DB_APP_PASSWORD}
```

php artisan migrate
# 必要に応じて、sdk など入れる
composer require aws/aws-sdk-php
composer require predis/predis
exit

5.アクセス確認
curl -v http://localhost
